package ch.zhaw.knn;


import org.junit.Test;

/**
 * Created by raphael on 12.01.14.
 */
public class TrainerTest {


    @org.junit.Test
    public void testGetTraniningValues() throws Exception {
     Trainer trainer = new Trainer();
        Perceptron perceptron = new Perceptron(trainer.getTraniningValues(2),2000);
    }

    @Test
    public void orTest(){
        Perceptron perceptron = new Perceptron();
        int[] input = new int[2];
        input[0] = 1;
        input[1] = 0;
        perceptron.logicalFunctions(input,1,200);
        input[0] = 0;
        input[1] = 0;
        perceptron.logicalFunctions(input,0,200);

        input[0] = 1;
        input[1] = 1;
        perceptron.logicalFunctions(input,1,200);
    }
    @Test
    public void andTest(){
        Perceptron perceptron = new Perceptron();
        int[] input = new int[2];
        input[0] = 1;
        input[1] = 0;
        perceptron.logicalFunctions(input,0,200);
        input[0] = 0;
        input[1] = 0;
        perceptron.logicalFunctions(input,0,200);

        input[0] = 1;
        input[1] = 1;
        perceptron.logicalFunctions(input,1,200);
    }
    @Test
    public void xorTest(){
        Perceptron perceptron = new Perceptron();
        int[] input = new int[2];
        input[0] = 1;
        input[1] = 0;
        perceptron.logicalFunctions(input,1,200);
        input[0] = 0;
        input[1] = 0;
        perceptron.logicalFunctions(input,0,200);

        input[0] = 1;
        input[1] = 1;
        perceptron.logicalFunctions(input,0,200);
    }
}
