package ch.zhaw.knn;


import java.util.*;

/**
 * Created by raphael on 12.01.14.
 */
public class Trainer {


    public String binNumber() {
        Random rg = new Random();
        int n = rg.nextInt(32767);
        return Integer.toBinaryString(n);
    }

    public Map<String,String> getTraniningValues(int count) throws Exception{
        if(count<0){
            throw new Exception("only positive Numbers allowed");
        }
       Map<String,String> results = new HashMap<String,String>();
        for(int n= 0; n<= count ;n++){
            String input = this.binNumber();
            String output = this.buildOutPut(input);
            results.put(input,output);
        }
        return results;
    }
            private String buildOutPut(String input){
                String output = "";
                for(int n = 0;n<input.length();n++){
                    if(input.charAt(n) == '1'){
                        output+="0";
                    }else{
                        output+="1";
                    }
                }
                return output;
            }
}
