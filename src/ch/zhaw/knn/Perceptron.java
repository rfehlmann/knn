package ch.zhaw.knn;


import java.util.Map;
import java.util.Random;

/**
 * Created by raphael on 12.01.14.
 */
public class Perceptron {

    int alpha = 0;
    int success = 0;
    int error = 0;
    double wight = 0.0;
    double level = 0.5;
    boolean correct = false;

    public Perceptron() {
    }

    Perceptron(Map<String, String> values, int trys) {
        this.initWight();
        this.initAlpha();
        for (String input : values.keySet()) {
            String output = values.get(input);

            for (int n = 0; n < input.length(); n++) {
                correct = false;
                int inputChar = Integer.valueOf(String.valueOf(input.charAt(n)));
                for (int d = 0; d < trys && !correct; d++) {

                    int generatedValue = this.calculateValue(inputChar);
                    int a = Integer.valueOf(String.valueOf(output.charAt(n)));
                    check(generatedValue, a);
                }
            }

        }
    }

    public boolean check(int input, int generate) {
        if (input == generate) {
            success++;
            System.out.println("success");
            correct = true;
        } else {
            error++;
            this.changeWigth(input, generate);
            System.out.println("error");

        }
        return  correct;
    }

    private void changeWigth(int input, int output) {
        if (input == 1) {
            wight += -(alpha * (input - output) * this.level);
        } else
            wight += +(alpha * (output - input) * this.level);

    }

    private void initWight() {
        Random rg = new Random();
        wight = rg.nextInt(1000) / 1000.0;

    }

    private void initAlpha() {
        Random rg = new Random();
        alpha = rg.nextInt(7) + 1;

    }

    private int calculateValue(int binaryValue) {
        if (binaryValue + this.wight >= level) {
            return 1;
        } else {
            return 0;
        }
    }

    public void logicalFunctions(int[] inputs, int output, int tries) {
        this.correct = false;
        boolean isCorrect = false;
        for (int n = 0; (n < tries && !isCorrect); n++) {
            int generated = 0;
            for (int i = 0; i < inputs.length; i++) {
            generated += inputs[i];
            }
            generated = this.calculateValue(generated);
            isCorrect = this.check(output,  generated);
        }
    }
}
